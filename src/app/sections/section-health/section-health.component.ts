import {Component, OnDestroy, OnInit} from '@angular/core';
import {Server} from '../../shared/server';
import {ServerService} from '../../services/server.service';
import {ServerMessage} from '../../shared/server-message';
import {AnonymousSubscription} from 'rxjs-compat/Subscription';
import {Observable, timer} from 'rxjs';

// const SAMPLE_SERVERS = [
//   {id:1, name: 'dev-web', isOnline: true},
//   {id:2, name: 'dev-mail', isOnline: true},
//   {id:3, name: 'prod-web', isOnline: false},
//   {id:4, name: 'prod-mail', isOnline: true}
// ];


@Component({
  selector: 'app-section-health',
  templateUrl: './section-health.component.html',
  styleUrls: ['./section-health.component.css']
})
export class SectionHealthComponent implements OnInit, OnDestroy {


  constructor(private serverService: ServerService) {
  }

  timerSubcription: AnonymousSubscription;

  servers: Server[] = [];

  ngOnInit() {
    this.refreshData();
  }

  ngOnDestroy() {
    if (this.timerSubcription) {
      this.timerSubcription.unsubscribe();
    }
  }

  refreshData() {
    this.serverService.getServers().subscribe(
      data => {
        console.log('Response: ', data);
        this.assignServers(data);
        console.log('Servers: ', this.servers);
      }
    );
    this.subscribeToData();
  }

  subscribeToData() {
    this.timerSubcription = timer(3000).subscribe(() => this.refreshData());
  }

  assignServers(data) {
    let i = 0;
    let servers: Server[] = [];
    while (i < data.length) {
      servers.push({id: data[i]['id'], name: data[i]['name'], isOnline: data[i]['isOnline']});
      i++;
    }
    this.servers = servers;
  }

  sendMessage(msg: ServerMessage) {
    this.serverService.handleServerMessage(msg).subscribe(data => console.log('Message sent to server: ', data),
      err => console.log('Error: ', err));
  }


}
