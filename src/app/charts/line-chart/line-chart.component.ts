import {Component, OnInit} from '@angular/core';
import {LINE_CHART_COLORS} from '../../shared/chart.colors';
import {SalesDataService} from '../../services/sales-data.service';
import * as moment from 'moment';
//
// const LINE_CHART_SAMPLE_DATA: any[] = [
//   {data: [23, 87, 31, 62, 73, 12], label: 'Sentiment Analysis'},
//   {data: [13, 81, 90, 14, 33, 18], label: 'Image Recognition'},
//   {data: [83, 23, 37, 12, 89, 77], label: 'Guest Registration'}
// ];
//
// const LINE_CHARTS_LABELS: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'];

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})


export class LineChartComponent implements OnInit {

  constructor(private salesService: SalesDataService) {
  }

  public topCustomers: string[];
  allOrders: any[];

  public lineChartData: any;
  public lineChartLabels: any;
  public lineChartOptions: any = {
    responsive: true,
    maintainAspectRatio: false
  };
  public lineChartLegend: true;
  public lineChartType = 'line';
  public lineChartColors = LINE_CHART_COLORS;

  ngOnInit() {
    this.salesService.getOrders(1, 100).subscribe(
      res => {
        this.allOrders = res['page']['data'];


        this.salesService.getOrdersByCustomer(3).subscribe(
          cus => {
            if (cus instanceof Array) {
              this.topCustomers = cus.map(x => x['name']);
            }


            const allChartData = this.topCustomers.reduce((result, i) => {
              result.push(this.getChartData(this.allOrders, i));
              return result;
            }, []);

            let dates = allChartData.map(x => x['data']).reduce((a, i) => {
              a.push(i.map(o => new Date(o[0])));
              return a;
            }, []);

            // console.log('dates:',dates);
            dates = [].concat.apply([], dates);
            const r = this.getCustomerOrdersByDate(allChartData, dates)['data'];


            this.lineChartLabels = r[0]['orders'].map(o => o['date']).reverse();

            this.lineChartData = [
              {'data': r[0]['orders'].map(x => x['total']), 'label': r[0]['customer']},
              {'data': r[1]['orders'].map(x => x['total']), 'label': r[1]['customer']},
              {'data': r[2]['orders'].map(x => x['total']), 'label': r[2]['customer']}
            ];
          }
        );
      }
    );


  }


  getChartData(allOrders: any, name: string) {
    const customerOrders = allOrders.filter(o => o.customer.name === name);
    const formattedOrders = customerOrders.reduce((r, e) => {
      r.push([e.placed, e.total]);
      return r;
    }, []);

    console.log(formattedOrders);

    const result = {customer: name, data: formattedOrders};
    console.log('result: ', result);
    return result;
  }

  getCustomerOrdersByDate(orders: any, dates: any) {
    const customers = this.topCustomers;
    const prettyDates = dates.map(x => LineChartComponent.toFriendlyDate(x));
    const u = Array.from(new Set(prettyDates)).sort();


    const result = {};
    const dataSets = result['data'] = [];

    customers.reduce((x, y, i) => {
      console.log('Reducing: ', y, '  at index:', i);
      const customerOrders = [];
      dataSets[i] = {
        customer: y, orders: u.reduce((r, e, j) => {
          console.log('Reducing: ', e, '  at index:', j);

          const obj = {};
          obj['date'] = e;
          obj['total'] = this.getCustomerDateTotal(e, y);
          customerOrders.push(obj);
          return customerOrders;
        }, [])
      };
      return x;
    }, []);

    return result;
  }

  getCustomerDateTotal(date: any, customer: string) {

    const r = this.allOrders.filter(o => o.customer.name === customer && LineChartComponent.toFriendlyDate(o.placed) === date);

    return r.reduce((a, b) => {
      return a + b.total;
    }, 0);
  }

  static toFriendlyDate(date: Date) {
    return moment(date).endOf('day').format('YY-MM-DD');
  }
}

