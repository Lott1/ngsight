import { Component, OnInit } from '@angular/core';
import {SalesDataService} from '../../services/sales-data.service';

@Component({
  selector: 'app-section-sales',
  templateUrl: './section-sales.component.html',
  styleUrls: ['./section-sales.component.css']
})
export class SectionSalesComponent implements OnInit {

  salesDataByCustomer: any;
  salesDataByState: any;

  constructor(private salesService: SalesDataService) { }


  ngOnInit() {
    this.salesService.getOrdersByCustomer(5).subscribe(
      data => {
        this.salesDataByCustomer = data;
      }
    );

    this.salesService.getOrdersByState().subscribe(
      data => {
        this.salesDataByState = data;
      }
    );
  }

}
