import {ThemeColors} from './theme.colors.model';

export const THEME_COLORS: ThemeColors[] = [

  {
    name: 'Default',
    colorSet: [
      '#104080',
      '#ff7070',
      '#f0d060',
      '#eea040',
      '#ff5030'
    ]
  },
  {
    name: 'Bright',
    colorSet: [
      '#26547c',
      '#ff6060',
      '#ffd166',
      '#06d6a0',
      '#fcfcfc'
    ]
  }

];
