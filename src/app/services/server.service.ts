import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Server} from '../shared/server';
import {ServerMessage} from '../shared/server-message';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServerService {



  private headers: HttpHeaders;
  private accessPointUrl = 'https://localhost:44376/api/servers';

  data: any;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({'Content-Type': 'application/json;charset=utf-8'});
  }

  getServers(): Observable<Server>{
    return this.http.get<Server>(this.accessPointUrl, {headers: this.headers});
  }

  handleServerMessage(message: ServerMessage): Observable<Response>{
    const url = this.accessPointUrl + '/' + message.id;
    return this.http.put<Response>(url, message, {headers: this.headers});
  }
}
