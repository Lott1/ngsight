import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SalesDataService {

  private headers: HttpHeaders;
  private accessPointUrl = 'https://localhost:44376/api/orders/';

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({'Content-Type': 'application/json;charset=utf-8'});
  }

  public getOrders(page, limit) {
    return this.http.get(this.accessPointUrl + page + '/' + limit, {headers: this.headers});
  }

  public getOrdersByCustomer(n: number) {
    return this.http.get(this.accessPointUrl + 'bycustomer' + '/' + n, {headers: this.headers});

  }

  public getOrdersByState() {
    return this.http.get(this.accessPointUrl + 'bystate', {headers: this.headers});

  }
}
