export const LINE_CHART_COLORS = [
  {
    backgroundColor: 'rgba(6,200,100,0.2)',
    borderColor: 'rgba(0,150,70,0.5)',
    pointBackgroundColor: '#000',
    pointBorderColor: '#000',
    pointHoverBackgroundColor: '#4ff',
    pointHoverBorderColor: '#600',
  },
  {
    backgroundColor: 'rgba(20,110,78,0.2)',
    borderColor: 'rgba(10,87,120,0.5)',
    pointBackgroundColor: '#000',
    pointBorderColor: '#000',
    pointHoverBackgroundColor: '#4ff',
    pointHoverBorderColor: '#600',
  },
  {
    backgroundColor: 'rgba(60,25,215,0.2)',
    borderColor: 'rgba(110,15,10,0.5)',
    pointBackgroundColor: '#000',
    pointBorderColor: '#000',
    pointHoverBackgroundColor: '#4ff',
    pointHoverBorderColor: '#600',
  }
];
